# Comp3100 - Distributed Systems #

### What is this repository for? ###

Ver. 1.1.42

This is a repositiory for the major assignment of Comp3100

### Setting up ds-sim ###
* Go into `ds-sim` and call make
* run with:
  * `./ds-server -n -v all`


### Running client scheduler ###
* Ensure ds-sim is built (above)
* Run ds-sim (above)
* Navigate to `/Scheduler`
* Run `make` to build
* Run `make run` to run


### Contribution Team ###

Team:

* Billy Mihalarias (45266026)
* Paige Anthony (45284679)
* Joel Morrissey (45238049)
