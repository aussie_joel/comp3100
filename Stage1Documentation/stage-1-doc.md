(`max. 5 pages` including everything; the entire submission is 5 pages or fewer)
# Title #

`list group members`
* Billy Mihalarias (45266026)
* Paige Anthony (45284679)
* Joel Morrissey (45238049)

## Introduction ##
`describe what stage 1 is about`

## System Overview ##
`high level description of system design`

`server-sim and client-side(focus)`

`reasoning for decissions, ouline contraints`

## System Architecture ##
`in depth system description`

`components design, functionality and relationships`

`subort with figures`

## Implementation Details ##
`list of sources/referneces implemented`

`technologies, libraries, data structures used`

`desription of implementation, functions, relations`

`reasoning and impact on design`

## References ##
`repo`