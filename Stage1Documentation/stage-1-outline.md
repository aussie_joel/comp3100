# Stage 1: 'vallina' client-side simulator with a simple job dispatcher (updated on 30th March) #

## A ‘vallina’ client-side simulator with a simple job dispatcher ##

This assessment item accounts for `15% of the total mark` (100) of the unit. As this simulator design and implementation is to be a `group work`, each group shall make only one submission (no individual submissions). The marking will be conducted in two steps:
1. `reviewing the submission (due Friday week 6)` 
2. `assessing the demo (at your allocated workshop in week 7)`.

Your main task in this stage is to design and implement a plain/vanilla client-side simulator that acts as a simple job dispatcher. This version of client-side simulator shall fully `implement the ds-sim simulation protocol` as described in the Project description. Briefly, it `connects the server-side simulator`, `receives jobs and schedule them all to the first one of largest server type` (e.g., ID 0 of large server type in ds-config1.xml); hence, the name of job dispatching policy is `‘allToLargest’`. **Appendix F in the Project description is a sample simulation** log of stage 1 implementation; note that your log might be different if you schedule jobs (SCHD) without requesting resource information (RESC). Your client-side simulator should work for any simulation configuration. A **sample client-side simulator (ds-client) provided in ds-sim.tar** can be used as a reference implementation. In particular, when running the server with ‘-v brief’, your simulation logs should exactly match those generated using the reference implementation.

* The largest server type is defined to be the one with the largest number of CPU cores.

* **The format and the order of messages are of importance.** For example, the order of 'JOBN', 'SCHD' and the actual execution of job. Job details, such as submission time, runtime and resource requirements should also match. If you run the server with ‘-v all’, ds-server might show log differently depending on how the client makes scheduling decisions.

 ---

## Deliverable ##

The submission is to be a `single compressed file` (in the format of .7z,.gz,.gzip,.rar,.tar,.zip) containing everything necessary to run the simulator with the default ‘allToLargest’ algorithm and simulator design document.

Two main components of your submission are as follows:
* `The source code` of your group’s client-side simulator
* `Simulator design document` (this is different from the project design document, stage 0)

Your submission may include other files, such as makefile or a shell script for installation.

In addition to your submission, your `group needs to run a demo` at an allocated workshop in week 7. The actual allocation for your demo will be done later, most likely sometime in week 6. The `detailed demo process` needs to be documented and attached to your submission, and you must follow this documentation in your demo.

 

Suggested headings for simulator design document:

(`max. 5 pages` including everything; the entire submission is 5 pages or fewer)

- **Project title:** no more than 100 characters (approximately 14 words), e.g., Cloud job scheduler or Cost-efficient resource allocator for distributed systems.

- **Group members:** full names and student numbers, e.g., Young Choon Lee (12345678).

- **Introduction:** What this project is about `focusing on stage 1`

- **System  overview:** high-level description of the system (both client-side simulator and server-side simulator with the `focus being your client-side simulator`) including design philosophy, considerations and constraints.

- **System architecture:** `description of the system` including components/functions of the system and their design and functionalities; preferably, with a `figure showing these components` and their relationships (this figure may be in System overview).

- **Implementation details:** brief `description of any implementation` specific information including technologies, techniques, software libraries and data structures used. How each of components/functions of your simulator is implemented including who is in charge of which function(s) and how they have led the design and development.

- **References:** including `Bitbucket` project repository/wiki

 ---

## Marking rubric (in percentage) ##

**85 to 100**

Work in this band presents a full and comprehensive account of all requirements outlined above. In particular, the efficient and elegant design and implementation of the fully functional client-side simulator with the correctly working allToLargest policy should be evident in (1) the source code, (2) design document and (3) demo. The clear demonstration of teamwork should be evident in all these three components, e.g., git commit history and all group members actively participating in the demo including confidently and sufficiently answering any questions from markers during the demo.

**75 to 84**

Work in this band presents a clear account of all requirements outlined above. In particular, the efficient and clear design and implementation of the fully functional client-side simulator with the correctly working allToLargest policy should be evident in (1) the source code, (2) design document and (3) demo. The sufficient demonstration of teamwork should be evident in all these three components, e.g., git commit history and participation in the demo.

**65 to 74**

Work in this band presents good design and implementation of the fully functional client-side simulator. The appropriate demonstration of teamwork in all three assessment components should be evident.

**50 to 64**

Work in this band presents an adequate design and implementation of the working client-side simulator. The appropriate demonstration of teamwork should be evident.