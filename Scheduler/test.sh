#!/bin/bash

src_prog="./src/"
bin_prog="./bin/"
sever_loc="../ds-sim/"
sever="ds-server"
client="ds-client"
package_name="Scheduler"
main_prog="Stage1"
server_argument_ours=" -n -v all"
server_argument_theirs=""
do_compile=false;
is_make=true;
make_loc="./configs"
algo=""
currect_dir=""$(pwd)

configDir="../ds-sim/configs/"
diff_folder="diff"


while [ "$1" != "" ] ; do
	case $1 in
		-wf )	shift
				algo="-a wf"
				;;
		-bf )	shift
				algo="-a bf"
				;;
		-ff )	shift
				algo="-a ff"
				;;
		-a	)	shift
				algo="-a "$1
				;;
	esac
	shift
done


diffLog="./"$diff_folder"/stage2diff.log"
our_output="./"$diff_folder"/ours.log"
their_output="./"$diff_folder"/theirs.log"

run="cd "$bin_prog"; java "$package_name"."$main_prog" "$algo"; cd "$currect_dir

if [ ! -d $diff_folder ]; then
	mkdir $diff_folder
fi

true > $diffLog

trap "kill 0" EXIT

for conf in $configDir*.xml; do
	printf "\nrunning config: $conf\n"
	printf "running your code...\n"
	eval "cd "$sever_loc
	eval "./"$sever$server_argument_ours" -c "$conf | sed -n -e '/RCVD QUIT/,$p' > $currect_dir/$our_output&
	eval "cd "$currect_dir
	sleep 1s
	eval $run

	printf "Running test client...\n"
	eval "cd "$sever_loc
	eval "./"$sever$server_argument_theirs" -v all -c "$conf | sed -n -e '/RCVD QUIT/,$p' > $currect_dir/$their_output&
	sleep 1s
	eval $sever_loc$client" "$algo
	eval "cd "$currect_dir

	eval "diff "$our_output $their_output > $diff_folder/"temp.log"
	if [ -s $diff_folder/"temp.log" ]; then
		printf "\nNOT PASSED! --> $conf\n"
		eval "cat "$our_output > "./"$diff_folder/"failed_ours_"$(basename $conf)".log"
		eval "cat "$their_output > "./"$diff_folder/"failed_expected_"$(basename $conf)".log"
	else
		printf "\nPASSED!\n"
	fi
	eval "cat "$diff_folder/temp.log >> $diffLog
done

if [ ! -s $diff_folder/$diffLog ]; then
	printf "ALL TESTS PASSED!\n"
fi