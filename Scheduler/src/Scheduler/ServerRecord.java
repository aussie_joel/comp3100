package Scheduler;

class ServerRecord {

    public int diskSpace;
    public int memory;
    public int cpuCores;
    public int availableTime;
    public int serverState;
    public int serverId;
    public String serverType;

    public ServerRecord (String record){
        String[] records = record.split(" ");
        set(records[0], records[1], records[2], records[3],
            records[4], records[5], records[6]);
    }

    public ServerRecord(String serverType, String serverId, String serverState,
        String availableTime,String cpuCores,String memory,String diskSpace){
            set(serverType, serverId, serverState, availableTime, cpuCores, memory, diskSpace);
    }

    private void set(String serverType, String serverId, String serverState,
        String availableTime,String cpuCores,String memory,String diskSpace){
            this.diskSpace = Integer.parseInt(diskSpace);
            this.memory = Integer.parseInt(memory);
            this.cpuCores = Integer.parseInt(cpuCores);
            this.availableTime = Integer.parseInt(availableTime);
            this.serverState = Integer.parseInt(serverState);
            this.serverId = Integer.parseInt(serverId);
            this.serverType = serverType;
    }

    public boolean canRunJob(JobRecord job) {
        return job.numOfCores <= this.cpuCores &&
               job.memory <= this.memory&&
               job.disk <= this.diskSpace;
    }

    public boolean isAvailable() {
        return availableTime==-1;
    }

    public boolean isActive() {
        return serverState==3;
    }

    @Override
    public String toString() {
        return serverType + " " + serverId + " " + serverState + " " + availableTime + " " + cpuCores + " " + memory + " " + diskSpace;
    }
}