package Scheduler;

public class BestFit implements Schedule {

	CommunicationSingleton com;
	int maxFailures = 10;

    public BestFit() {
        com = CommunicationSingleton.getInstance();
	}

    @Override
	public void run() {
		//for all jobs
		for (	Response nextJob = getNextJob(com);
				!nextJob.getStatus().equalsIgnoreCase("NONE");
				nextJob = getNextJob(com)){

            
            JobRecord job = new JobRecord(nextJob.getLine());

            ServerRecord server = bestBaseOnActiveState(job);
            
            if(server != null){
                com.get("SCHD", job.jobID, server.serverType, server.serverId);
            } else {
                SystemXMLRecord serverXml = bestBaseOninitState(job);
                com.get("SCHD", job.jobID, serverXml.type, 0);
            }
        }			
    }

    public ServerRecord bestBaseOnActiveState(JobRecord job) {
        int bestFit = Integer.MAX_VALUE;
        ServerRecord best = null;
        int minAvail = Integer.MAX_VALUE;
        SystemXMLList serverXML = getSystemXMLClassOrdered();
        ServerList servers = new ServerList(retrieveCapableServers(job,com).getData());

        for (SystemXMLRecord type: serverXML) {
            for (ServerRecord s: servers.getAllServersOfType(type.type)) {
                if (s.canRunJob(job)) {
                    int fitness = fitness(s, job);
                    if(fitness < bestFit || (fitness==bestFit && s.availableTime<minAvail)) {
                        bestFit=fitness;
                        best = s;
                        minAvail=s.availableTime;
                    }
                }
            }
        }
        return best;
    }

    public SystemXMLRecord bestBaseOninitState(JobRecord job) {
        int bestFit = Integer.MAX_VALUE;
        SystemXMLRecord best = null;
        SystemXMLList serverXML = getSystemXMLClassOrdered();

        for (SystemXMLRecord type: serverXML) {
            if (type.canRunJob(job)) {
                int fitness = fitness(type, job);
                if(fitness < bestFit) {
                    bestFit=fitness;
                    best = type;
                }
            }
        }
        return best;
    }
}