package Scheduler;

import java.util.*;

public class SystemXMLList implements Iterable<SystemXMLRecord> {

    ArrayList<SystemXMLRecord> records = new ArrayList<SystemXMLRecord>();

    public SystemXMLList(HashMap<String,HashMap<String, String>> data) {
        for (String x: data.keySet()) {
            records.add(new SystemXMLRecord(x, data.get(x)));
        }
    }

    public SystemXMLList(LinkedHashMap<String,LinkedHashMap<String, String>> data) {
        for (String x: data.keySet()) {
            records.add(new SystemXMLRecord(x, data.get(x)));
        }
    }

    public SystemXMLRecord getMostCores() {
        SystemXMLRecord out = records.get(0);
        for (SystemXMLRecord x: records) {
            if (out.coreCount < x.coreCount) {
                out = x;
            }
        }
        return out;
    }

    public ArrayList<SystemXMLRecord> orderedSmallestToLargest() {
        ArrayList<SystemXMLRecord> ret = new ArrayList<SystemXMLRecord>();
        for (SystemXMLRecord x: records) {
            ret.add(x);
        }
        Collections.sort(ret, new Comparator<SystemXMLRecord>() {
            @Override
            public int compare(SystemXMLRecord o1, SystemXMLRecord o2) {
                if (o1.coreCount > o2.coreCount) {
                    return 1;
                } else if (o1.coreCount < o2.coreCount) {
                    return -1;
                }
                return 0;
            }
        });
        return ret;
    }

    public SystemXMLRecord get(int idx) {
        return records.get(idx);
    }

    public int size() {
        return records.size();
    }

    public String toString() {
        return records.toString();
    }

    @Override
    public Iterator<SystemXMLRecord> iterator() {
        return records.iterator();
    }
}