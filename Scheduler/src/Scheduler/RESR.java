package Scheduler;
public class RESR implements Command {

	public class RecoverResponse extends Response {	
		String recoverInfo;

		public RecoverResponse(String recoverInfo){
			this.recoverInfo = recoverInfo;
		}

		@Override
		public String getLine(){
			return recoverInfo;
		}
	}

	@Override
	public Response execute(String input) {
		return new RecoverResponse(input);
	}
	
}
