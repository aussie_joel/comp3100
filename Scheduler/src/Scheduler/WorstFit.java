package Scheduler;

public class WorstFit implements Schedule {

    SystemXMLList serverXML = getSystemXMLClass();

    CommunicationSingleton com;

    public WorstFit() {
        com = CommunicationSingleton.getInstance();
    }

    @Override
	public void run() {
		//for all jobs
		for (	Response nextJob = getNextJob(com);
				!nextJob.getStatus().equalsIgnoreCase("NONE");
				nextJob = getNextJob(com)){

            
            JobRecord job = new JobRecord(nextJob.getLine());

            ServerRecord server = worstBaseOnActiveState(job);
            
            if(server != null){
                com.get("SCHD", job.jobID, server.serverType, server.serverId);
            } else {
                SystemXMLRecord serverXml = WorstBaseOninitState(job);
                com.get("SCHD", job.jobID, serverXml.type, 0);
            }
        }			
    }

    public ServerRecord worstBaseOnActiveState(JobRecord job) {
        int worstFit = Integer.MIN_VALUE;
        ServerRecord worst = null;
        int altFit = Integer.MIN_VALUE;
        ServerRecord alt = null;
        SystemXMLList serverXML = getSystemXMLClassOrdered();
        ServerList servers = new ServerList(retrieveCapableServers(job,com).getData());

        for (SystemXMLRecord type: serverXML) {
            for (ServerRecord s: servers.getAllServersOfType(type.type)) {
                if (s.canRunJob(job)) {
                    int fitness = fitness(s, job);
                    if(fitness > worstFit && (s.serverState==3||s.serverState==2)) {
                        worstFit = fitness;
                        worst = s;
                    } else if(fitness > altFit && s.serverState!=4) {
                        altFit = fitness;
                        alt = s;
                    }
                }
            }
        }
        return (worst != null ? worst:alt);
    }

    public SystemXMLRecord WorstBaseOninitState(JobRecord job) {
        int worstFit = Integer.MIN_VALUE;
        SystemXMLRecord worst = null;
        int altFit = Integer.MIN_VALUE;
        SystemXMLRecord alt = null;
        SystemXMLList serverXML = getSystemXMLClass();
        
        for (SystemXMLRecord s: serverXML) {
            if (s.canRunJob(job)) {
                int fitness = fitness(s, job);
                if(fitness > worstFit) {
                    worstFit = fitness;
                    worst = s;
                } else if(fitness > altFit) {
                    altFit = fitness;
                    alt = s;
                }
            }
        }
        return (worst != null ? worst:alt);
    }
}

/*
For	a	given	job	ji,
1.	Set	worstFit	and	altFit	to	very	small	number	(e.g.,	INT_MIN)
2.	Obtain	server	state	information
3.	For	each	server	type	i,	si	,	in	the	order	appear	in	system.xml
4.	For	each	server	j,	si,j	of	server	type	si,	from	0	to	limit	- 1
5.	If	server	si,j	has	sufficient	resources	readily	available	to	run	job	ji	then
6.	Calculate	the	fitness	value	fsi,j,ji
7.	If	fsi,j,ji	>	worstFit	and	si,j	is	immediately	available	then
8.	Set	worstFit	to	fsi,j,ji
9.	Else	If	fsi,j,	ji	>	altFit	and	si,j	is	available	in	a	short	definite	amount	of	time	
then
10.	Set	altFit	to	fsi,j,	ji
11.	End	If
12.	End	If
13.	End	For
14.	End	For
15.	If	worstFit	is	found	then
16.	Return	the	server	with	worstFit
17.	Else	If	altFit	is	found	then
18.	Return	the	server	with	altFit
19.	Else
20.	Return	the	worst-fit	Active	server	based	on	initial	resource	capacity
21.	End	If

*/