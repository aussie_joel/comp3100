package Scheduler;

import java.util.*;

public class FirstFit implements Schedule {
 
    CommunicationSingleton com;

    public FirstFit() {
        com = CommunicationSingleton.getInstance();
    }
    
    @Override
    public void run() {
        
        //for all jobs
        for (	Response nextJob = getNextJob(com);
                !nextJob.getStatus().equalsIgnoreCase("NONE");
                nextJob = getNextJob(com)){

            JobRecord job = new JobRecord(nextJob.getLine());

            ServerRecord server = getFirstBasedOnCurrentCapability(job);
            
            if(server != null){
                com.get("SCHD", job.jobID, server.serverType, server.serverId);
            } else {
                ServerRecord altServer = getFirstBasedOnInitCapability(job);
                com.get("SCHD", job.jobID, altServer.serverType, altServer.serverId);
            }

        }

    }

    public ServerRecord getFirstBasedOnCurrentCapability(JobRecord job) {
        ArrayList<SystemXMLRecord> list = getSystemXMLClass().orderedSmallestToLargest();
        ServerList servers = new ServerList(retrieveCapableServers(job,com).getData());

        for (SystemXMLRecord type: list) {
            for (ServerRecord s: servers.getAllServersOfType(type.type)) {
                if(s.cpuCores >= job.numOfCores){
                    return s;
                }
            }
        }
        return null;
    }

    public ServerRecord getFirstBasedOnInitCapability(JobRecord job) {
        ArrayList<SystemXMLRecord> list = getSystemXMLClass().orderedSmallestToLargest();
        ServerList servers = new ServerList(retrieveCapableServers(job,com).getData());

        for (SystemXMLRecord type: list) {
            for (ServerRecord s: servers.getAllServersOfType(type.type)) {
                if(type.canRunJob(job)) {
                    return s;
                }
            }
        }
        return null;
    }
}