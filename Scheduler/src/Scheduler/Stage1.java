package Scheduler;

import java.util.ArrayList;
import java.util.Arrays;

public class Stage1 {
	
	CommunicationSingleton com = CommunicationSingleton.getInstance();
	
	public static void main(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-h"))) {
			printUsage();
			System.exit(0);
		}
		new Stage1(new ArrayList<String>( Arrays.asList(args)));
	}
	
	public Stage1(ArrayList<String> args) {
		handShake();
		
		scheduleJobs(args);

		exit();
	}

	public void handShake() {
		com.get("HELO");
		com.get("AUTH Team34");
		System.out.println("handshake " + "\u001B[32m" + "complete" + "\u001B[0m");
	}

	public void scheduleJobs(ArrayList<String> args) {
		System.out.println("scheduling jobs ...");
		Schedule sch = new AllToLargest();

		if(args.contains("-a")) {
			int idx = args.indexOf("-a")+1;
			if (idx>args.size()) {
				invalidArgumentWarning("-a",(idx<args.size() ? args.get(idx):""),"AllToLargest strategy");
				printUsage();
			} else if (args.get(idx).equalsIgnoreCase("BF")) {
				sch = new BestFit();
				System.out.println("Running Best Fit");
			} else if (args.get(idx).equalsIgnoreCase("WF")) {
				sch = new WorstFit();
				System.out.println("Running Worst Fit");
			} else if (args.get(idx).equalsIgnoreCase("FF")) {
				sch = new FirstFit();
				System.out.println("Running First Fit");
			} else {
				invalidArgumentWarning("-a",(idx<args.size() ? args.get(idx):""),"AllToLargest strategy");
				printUsage();
			}
		}
		
		sch.run();
		System.out.println("job scheduling " + "\u001B[32m" + "complete" + "\u001B[0m");
	}

	public void exit()  {
		com.sendMessage("QUIT");
		com.recMessage();
		System.out.println("connection ended");
	}

	public static void invalidArgumentWarning(String option, String argument, String def) {
		System.out.print("\u001B[31m");
		if (def != null) {
			System.out.print("Invalid argument given (" + argument + ") for " + option + " option defaulting to " + def);
		} else {
			System.out.print("Invalid argument given (" + argument + ") for -a option");

		}
		System.out.print("\u001B[0m\n");
	}

	public static void printUsage() {

		System.out.println("Usage: java Scheduler.Stage1" + " [Options]");

		System.out.println("ALGORITHM SPECIFICATION:");
		System.out.println("\t-a <Algorithm>: The algorithm to Schedual jobs");
		System.out.println("\t\tEx: -a FF, -a WF, -a BF");

		System.out.println("MISC:");
		System.out.println("\t-h: Print this help summary page.");

		System.out.println("EXAMPLES:");
		System.out.println("\tjava Scheduler.Stage1 -a FF");
		System.out.println("\tjava Scheduler.Stage1 -a WF");
		System.out.println("\tjava Scheduler.Stage1 -a BF");
	}

	/**** Helper methods ****/
	public void print(Object ... args) {
		for (int i = 0; i<args.length-1; i++) {
			System.out.print(args[i] + " ");
		}
		System.out.println(args[args.length-1]);
	}
}