package Scheduler;

import java.util.HashMap;

public class SystemXMLRecord {
    public String type;
    public int limit;
    public int bootupTime;
    public float hourlyRate;
    public int coreCount;
    public int memory;
    public int disk;

    public SystemXMLRecord(String type, HashMap<String, String> map) {
        this(
            type,
            Integer.parseInt(map.get("limit")), 
            Integer.parseInt(map.get("bootupTime")), 
            Float.parseFloat(map.get("rate")), 
            Integer.parseInt(map.get("coreCount")), 
            Integer.parseInt(map.get("memory")), 
            Integer.parseInt(map.get("disk"))
        );
    }


    public SystemXMLRecord(String type, int limit, int bootupTime, float hourlyRate, int coreCount, int memory, int disk) {
        this.type = type;
        this.limit = limit;
        this.bootupTime = bootupTime;
        this.hourlyRate = hourlyRate;
        this.coreCount = coreCount;
        this.memory = memory;
        this.disk = disk;
    }

    public boolean canRunJob(JobRecord job) {
        return job.numOfCores <= this.coreCount &&
               job.memory <= this.memory&&
               job.disk <= this.disk;
    }

    public String toString() {
        return type;
    }

}