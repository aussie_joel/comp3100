package Scheduler;
import java.util.*;

class CommandInvoker {

    private static HashMap<String, Command> cmd  = new HashMap<String, Command>(Map.of(
        "OK", new OK(),
        "JOBN", new JOBN(),
        "RESR", new RESR(),
        "RESF", new RESF(),
        "JOBP", new JOBP(),
        "NONE", new NONE(),
        "DATA", new DATA(),
        "ERR:", new ERR()
    ));
    
    public void register(String commandName, Command command) {
        cmd.put(commandName.toUpperCase(), command);
    }
    
    public Response execute(String input) {
        Command command = cmd.get(input.split(" ")[0].toUpperCase());
        if (command == null) {
            throw new IllegalStateException("no command registered for " + input.split(" ")[0].toUpperCase());
        }
        return command.execute(input);
    }
}
