package Scheduler;
public class RESF implements Command {

	public class FailureResponse extends Response {	
		String recoverInfo;

		public FailureResponse(String recoverInfo){
			this.recoverInfo = recoverInfo;
		}

		@Override
		public String getLine(){
			return recoverInfo;
		}
	}

	@Override
	public Response execute(String input) {
		return new FailureResponse(input);
	}

}
