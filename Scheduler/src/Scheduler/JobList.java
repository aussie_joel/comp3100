package Scheduler;
import java.util.*;

public class JobList implements Iterable<JobRecord> {

    ArrayList<JobRecord> jobs = new ArrayList<JobRecord>();

    public JobList(String jobInput){
        jobs.add(new JobRecord(jobInput));
    }

    public JobList(ArrayList<String> jobInputs) {
        for (String x : jobInputs){
            jobs.add(new JobRecord(x));
        }
    }

    public JobRecord get(int idx) {
        return jobs.get(idx);
    }

    public int size() {
        return jobs.size();
    }

    @Override
    public Iterator<JobRecord> iterator() {
        return jobs.iterator();
    }

}