package Scheduler;
public class JOBP implements Command {

	public class JobProblem extends Response{

		String jobError;

		public JobProblem(String jobError){
			this.jobError = jobError;
		}

		@Override
		public String getLine(){
			return jobError;
		}
	}

	@Override
	public Response execute(String input) {
		return new JobProblem(input);
	}
	
}
