package Scheduler;
import java.util.*;
import java.util.stream.Collectors;

public class ServerList implements Iterable<ServerRecord> {

    public ArrayList<ServerRecord> servers = new ArrayList<ServerRecord>();

    private ServerList(){
    }

    public ServerList(ArrayList<String> response){
        for (String x : response){
            this.servers.add(new ServerRecord(x));
        }
    }

    public HashMap<String,ServerRecord> getNameMap(){
        return new HashMap<String,ServerRecord>(
            servers.stream().collect(Collectors.toMap(x -> x.serverType, x->x))  
        );
    }

    public static ServerList fromRecords(List<ServerRecord> records) {
        ServerList list = new ServerList();
        list.servers.addAll(records);
        return list;
    }

    public ServerRecord getMostCores() {
        ServerRecord out = servers.get(0);
        for (ServerRecord x: servers) {
            if (out.cpuCores < x.cpuCores) {
                out = x;
            }
        }
        return out;
    }

    public ArrayList<ServerRecord> getAllServersOfType(String type){
        ArrayList<ServerRecord> serversOfType = new ArrayList<ServerRecord>();
        for (ServerRecord s: servers) {
            if (s.serverType.equalsIgnoreCase(type)) {
                serversOfType.add(s);
            }
        }
        return serversOfType;
    }

    public ServerRecord get(int idx) {
        return servers.get(idx);
    }

    public int size() {
        return servers.size();
    }

    public String toString() {
        return servers.toString();
    }

    @Override
    public Iterator<ServerRecord> iterator() {
        return servers.iterator();
    }

}