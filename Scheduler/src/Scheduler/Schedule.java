package Scheduler;

import java.util.*;

public interface Schedule {

    public void run();

    default public HashMap<String,HashMap<String, String>> getSystemXML() {
        return new XMLParser().readXML();
    }
    
    default public HashMap<String,SystemXMLRecord> getSystemMap(){
        HashMap<String,HashMap<String, String>> data = getSystemXML();
        HashMap<String,SystemXMLRecord> records = new HashMap<String,SystemXMLRecord>();
        for (String x: data.keySet()) {
            records.put(x,new SystemXMLRecord(x, data.get(x)));
        }
        return records;
    }

    default public SystemXMLList getSystemXMLClass() {
        return new SystemXMLList(new XMLParser().readXML());
    }

    default public SystemXMLList getSystemXMLClassOrdered() {
        return new SystemXMLList(new XMLParser().readXMLOrdered());
    }

    default public Response retrieveAllServers(JobRecord  job, CommunicationSingleton com){
        return retrieveServersArgs(job,com,10,"All");
    }

    default public Response retrieveAvailableServers(JobRecord  job, CommunicationSingleton com){
        return retrieveServersArgs(job, com, 10, "Avail");
    }

    default public Response retrieveCapableServers(JobRecord  job, CommunicationSingleton com){
        return retrieveServersArgs(job,com,10,"Capable");
    }

    default public Response retrieveServersArgs(JobRecord  job, CommunicationSingleton com, int maxFailures,String argument) {
		Response resp = resourceInfoRequest(job, argument, com);

		int errorCount = 0;
		String stat = resp.getStatus();
		while (stat != null && stat.equalsIgnoreCase("ERR")) {
			System.err.println("Error: received error while retrieving server information");
			if (errorCount > maxFailures) {
				com.sendMessage("QUIT");
				com.recMessage();
				System.err.println("Error: receive more then: " + maxFailures + " from server");
				System.exit(1);
			}
			errorCount++;
            resp = resourceInfoRequest(job, argument, com);

			stat = resp.getStatus();
		}
		return resp;
    }

    default public Response resourceInfoRequest(JobRecord job, String Argument, CommunicationSingleton com){
        return com.get("RESC", Argument, job.numOfCores, job.memory, job.disk);
    }
    
    default public Response getNextJob(CommunicationSingleton com){
        return com.get("REDY");
    }

    default public int fitness(ServerRecord server, JobRecord job) {
        return server.cpuCores - job.numOfCores;
    }

    default public int fitness(SystemXMLRecord server, JobRecord job) {
        return server.coreCount - job.numOfCores;
    }
}