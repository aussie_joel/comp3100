package Scheduler;
import java.util.*;

public class DATA implements Command{

	public class DataResponse extends Response{	
		ArrayList<String> data;

		public DataResponse(ArrayList<String> data){
			this.data = data;
		}

		@Override
		public ArrayList<String> getData(){
			return data;
		}
	}



	@Override
	public DataResponse execute(String input) {
		CommunicationSingleton com = CommunicationSingleton.getInstance();
		com.sendMessage("OK");
		
		ArrayList<String> servers = new ArrayList<String>();
		String message = com.recMessage();
		while (!message.equals(".")) {
			servers.add(message);
			com.sendMessage("OK");
			message = com.recMessage();
		}

		return new DataResponse(servers);
	}
}
