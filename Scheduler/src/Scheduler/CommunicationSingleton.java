package Scheduler;

import java.io.*;
import java.net.*;
import java.util.*;

public class CommunicationSingleton {

	private static CommunicationSingleton instance;
	private int port = 50000;
	private String host = "127.0.0.1";
	private Socket s;
	private DataOutputStream out;
	private Scanner in;
	private CommandInvoker runCommand = new CommandInvoker();

	private CommunicationSingleton() {
		try {
			s = new Socket(host,port);
		} catch (UnknownHostException e) {
			System.err.println("Unable to connect to host IP: " + host + " not a valid ID\n");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IO error has occured when connect to socket with information -> IP: " + host + " Port: " + port + "\n");
			e.printStackTrace();
		}
		
		try {
			out = new DataOutputStream(s.getOutputStream());
			
			in = new Scanner(s.getInputStream());
		} catch (IOException e) {
			System.err.println("IO Error creating in and/or out streams");
			e.printStackTrace();
		}


		try {
			Runtime.getRuntime().addShutdownHook(new Thread(() -> {
					shutDown();
			}, "Safe-Communication-Shutdown-hook"));
		} catch (Exception e) {
			System.err.println("Warning: Failed to provide safe communication shutdown protocol");
		}
	}

	public static CommunicationSingleton getInstance() {
		if (instance == null) {
			instance = new CommunicationSingleton();
		}
		return instance;
	}

	private void shutDown() {
		try {
			in.close(); out.close(); s.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(String message) {
		message = formatMessage(message);
		try {
			out.write(message.getBytes());
		} catch (IOException e) {
			System.err.println("Error while trying to send message");
			e.printStackTrace();
		}
	}
	
	public void sendMessage(Object ... args) {
		String message = "";
		for (int i = 0; i<args.length; i++) {
			message += args[i].toString() + " ";
		}
		message = message.substring(0, message.length()-1);
		sendMessage(message);
	}

	public String recMessage() {
		return in.nextLine();
	}

	public String sendGetResponse(Object ... messages){
		sendMessage(messages);
		return recMessage();
	}

	public Response get(Object ... args) {
		String resp = CommunicationSingleton.getInstance().sendGetResponse(args);
		return runCommand.execute(resp);
	}

	public String formatMessage(String message) {
		while (message.endsWith("\n")) {
			message = message.substring(0, message.length()-1);
		}
		message += "\n";
		return message;
	}
}
