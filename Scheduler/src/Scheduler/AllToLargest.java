package Scheduler;

public class AllToLargest implements Schedule {

	CommunicationSingleton com;

    public AllToLargest() {
        com = CommunicationSingleton.getInstance();
	}

    @Override
    public void run() {
        SystemXMLList serverXML = getSystemXMLClass();
        
		//for all jobs
		for (	Response nextJob = getNextJob(com);
				!nextJob.getStatus().equalsIgnoreCase("NONE");
				nextJob = getNextJob(com)){

			JobRecord job = new JobRecord(nextJob.getLine());

            ServerList servers = new ServerList(retrieveAllServers(job,com).getData());
            
			SystemXMLRecord topServer = topServer(serverXML);
			com.get("SCHD", job.jobID, topServer.type, 0);

		}
	}

	public SystemXMLRecord topServer(SystemXMLList serverXML) {
		return serverXML.getMostCores();
	}

}