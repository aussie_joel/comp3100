package Scheduler;
public interface Command {
	Response execute(String input);
}
