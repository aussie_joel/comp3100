package Scheduler;

public class JobRecord {
    public String command;
    public int subTime;
    public int jobID;
    public int estRuntime;
    public int numOfCores;
    public int memory;
    public int disk;

    public JobRecord(String jobInput) {
        String[] record = jobInput.split(" ");
        set(record[0],record[1],record[2],record[3],record[4],record[5],record[6]);
    }

    public void set (String command, String subTime, String jobID, String estRunTime, String numOfCores, String memory, String disk) {
        this.command = command;
        this.subTime = Integer.parseInt(subTime);
        this.jobID = Integer.parseInt(jobID);
        this.estRuntime = Integer.parseInt(estRunTime);
        this.numOfCores = Integer.parseInt(numOfCores);
        this.memory = Integer.parseInt(memory);
        this.disk = Integer.parseInt(disk);
    }

    @Override
    public String toString() {
        return command + " " + subTime + " " + jobID + " " + estRuntime + " " + numOfCores + " " + memory + " " + disk;
    }
}