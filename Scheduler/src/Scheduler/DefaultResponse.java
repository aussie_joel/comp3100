package Scheduler;

public class DefaultResponse extends Response {
    String resp;

    public DefaultResponse(){
        this("OK");
    }
    public DefaultResponse(String resp){
        this.resp = resp;
    }

    @Override
    public String getStatus(){
        return resp;
    }
}