package Scheduler;

import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import java.util.*;

public class XMLParser {
    String filePath = "../../ds-sim/system.xml";
    // String filePath = "../../stage1tester_updatedV3/system.xml";
    String tagToRecieve = "server";
    ArrayList<String> attributesToRecieve = new ArrayList<String>(
        Arrays.asList("type", "limit","bootupTime","rate", "coreCount", "memory", "disk")
    );

    /***
     * Assumption is to use class default paths and information
    ***/
    public XMLParser() {
    }

   public XMLParser(String filePath, String tagToRecieve, ArrayList<String> attributesToRecieve) {
       this.filePath = filePath;
       this.tagToRecieve = tagToRecieve;
       this.attributesToRecieve = attributesToRecieve;
   }

   public LinkedHashMap<String,LinkedHashMap<String, String>> readXMLOrdered() {
    try {
        LinkedHashMap<String ,LinkedHashMap<String, String>> data = new LinkedHashMap<String,LinkedHashMap<String, String>>();

        File inputFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();


        NodeList nList = doc.getElementsByTagName(tagToRecieve);
        for (int temp = 0; temp < nList.getLength(); temp++) {
           Element eElement = (Element) nList.item(temp);

           LinkedHashMap<String, String> tempInfo = new LinkedHashMap<String, String>();
           for (String s: attributesToRecieve) {
               tempInfo.put(s, eElement.getAttribute(s));
           }
           data.put(tempInfo.get("type"),tempInfo);
        }
        return data;
     } catch (Exception e) {
        e.printStackTrace();
     }
     return null;
   }

   public HashMap<String,HashMap<String, String>> readXML() {
    try {
        HashMap<String ,HashMap<String, String>> data = new HashMap<String,HashMap<String, String>>();

        File inputFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();


        NodeList nList = doc.getElementsByTagName(tagToRecieve);
        for (int temp = 0; temp < nList.getLength(); temp++) {
           Element eElement = (Element) nList.item(temp);

           HashMap<String, String> tempInfo = new HashMap<String, String>();
           for (String s: attributesToRecieve) {
               tempInfo.put(s, eElement.getAttribute(s));
           }
           data.put(tempInfo.get("type"),tempInfo);
        }
        return data;

     } catch (Exception e) {
        e.printStackTrace();
     }
     return null;
   }
}