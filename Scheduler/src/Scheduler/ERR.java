package Scheduler;
public class ERR implements Command {
	public class JobError extends Response{

		String jobError;

		public JobError(String jobError){
			this.jobError = jobError;
		}

		@Override
		public String getLine(){
			return jobError;
		}
	}

	@Override
	public Response execute(String input) {
		return new JobError(input);
	}
}