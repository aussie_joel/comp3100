package Scheduler;
public class JOBN implements Command {

	public class JobResponse extends Response{	
		String line;

		public JobResponse(String line){
			this.line = line;
		}

		@Override
		public String getLine(){
			return line;
		}
	}

	@Override
	public Response execute(String input) {
		return new JobResponse(input);
	}
}
