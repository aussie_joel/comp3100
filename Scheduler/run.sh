#!/bin/bash

src_prog="./src/"
bin_prog="./bin/"
sever_loc="../ds-sim/"
sever="ds-server"
package_name="Scheduler"
main_prog="Stage1"
algo=""
server_argument="-n -v all"
is_mac=true;
do_compile=false;
is_make=true;
make_loc="./"
run_server=true;

printHelp () {
        printf "Usage: $0 [Options]\n"
        printf "SET FILE INFO:\n"
        printf "\t-src <file location>: Change the default file location to compile from\n"
        printf "\t-dst <file location>: Change the default file location to compile to\n"
        printf "\t-s <file name>: Set the server name to find and run\n"
        printf "\t-p <file name>: Set the package name to find and run/compile\n"
        printf "\t-m <file name>: Set the main file name to find and run\n"
        printf "ARGUMENT SPECIFICATION:\n"
        printf "\t-ca <argument>: A string of arguments to give to the scheduler program\n"
        printf "\t-sa <argument>: A string of arguments to give to the server program\n"
        printf "SYSTEM SPECIFICATION:\n"
        printf "\t-u: Run the program on a ubuntu box\n"
        printf "\t--unix: Run the program on a ubuntu box\n"
        printf "COMPILING FLAGS:\n"
        printf "\t-c: Set flag to compile before running the program\n"
        printf "\t-nm: Set flag that if we compile to not use make file\n"
        printf "CONTROL FLAGS:\n"
        printf "\t-ns: Don't run the server.\n"
        printf "MISC:\n"
        printf "\t-h: Print this help summary page.\n"
        printf "EXAMPLES:\n"
        printf "\t"$0" -ca \" -a wf\"\n"
        printf "\t"$0" -sa \" -n -v all -c some_config_file.xml\"\n"
        printf "\t"$0" -u -c -ca \" -a wf\"\n"
}

# get user info
while [ "$1" != "" ]; do
    case $1 in
        -src )  shift
                src_prog=$1
                ;;
        -dst )  shift
                bin_prog=$1
                ;;
        -s )    shift
                sever=$1
                ;;
        -p )    shift
                package_name=$1
                ;;
        -m )    shift
                main_prog=$1
                ;;
        -ca )   shift
                algo=$1
                ;;
        -sa )   shift
                server_argument=$1
                ;;
        -u | --unix )   is_mac=false
                        ;;
        -c )    do_compile=true
                ;;
        -nm )   is_make=false
                ;;
        -ns )   run_server=false
                ;;
        -h )    printHelp
                exit 0
                ;;
    esac
    shift
done

trap "kill 0" EXIT

#setup useful commands
open_new_terminal_linux_and_run_start="gnome-terminal -x bash -c \""
open_new_terminal_linux_and_run_end="; exec bash\""
open_new_terminal_mac_and_run_start="osascript -e 'tell application \"Terminal\" to do script \""
open_new_terminal_mac_and_run_end="\"'"
compile="javac -d "$bin_prog" "$src_prog$package_name"/*.java"
run="cd "$bin_prog"; java "$package_name"."$main_prog" "$algo
clear="clear"
make="cd "$make_loc"; make"

if [ "$do_compile" = true ] ; then
    if [ "$is_make" = true ] ; then
        eval $make
    else
        eval $compile
    fi
fi

if [ "$is_mac" = true ] ; then
    #run the server
    if [ "$run_server" = true ] ; then
        command="cd "$(pwd)"/"$sever_loc" && ""./"$sever" "$server_argument
        eval $open_new_terminal_mac_and_run_start$command$open_new_terminal_mac_and_run_end
        sleep 5s
    fi
    #run the client
    eval $run
else
    #run the server
    if [ "$run_server" = true ] ; then
        command="cd "$(pwd)"/"$sever_loc" && ""./"$sever" "$server_argument
        eval $open_new_terminal_linux_and_run_start$command$open_new_terminal_linux_and_run_end
        sleep 5s
    fi
    #run the client
    eval $run
fi
